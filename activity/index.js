

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendList = [];

function register(username){
    if(registeredUsers.indexOf(username)!== -1){
        alert("Registration failed. Username already exists!");
    } else {
        registeredUsers.push(username);
        alert("Thank you for registering!");
    }
}
function addFriend(userName){
    let foundUser = registeredUsers.indexOf(userName);
    if(foundUser !== -1){
        friendList.push(userName);
        alert("You have added " + userName + " as a friend!")
    } else {
        alert("User not found.");
    }
}


function displayFriends(){
    if (friendList.length === 0){
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendList.forEach(function(friend){
            console.log(friend);
        })
        }
    }


function displayNumberOfFriends(){
    if(friendList.length === 0){
        alert("You currently have 0 friends. Add one first.");
    
    } else {
        alert("You currently have " + friendList.length + " friends.")
    }
}

function deleteLastFriend(){
    if(friendList.length === 0){
        alert("You currently have 0 friends. Add one first.");
    
    } else {
       friendList.pop();
    }
}

function deleteFriends(indexUser, numberFriend){
    if(friendList.length === 0){
        alert("You currently have 0 friends. Add one first.");
    
    } else {
       friendList.splice(indexUser, numberFriend);
    }
}







